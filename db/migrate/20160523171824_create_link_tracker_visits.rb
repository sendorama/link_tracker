# This migration comes from link_tracker (originally 20160523171824)
class CreateLinkTrackerVisits < ActiveRecord::Migration
  def change
    create_table :link_tracker_visits do |t|
      t.belongs_to :link, index: true
      t.string :ip_address
      t.string :user_agent
      t.string :ua_browser
      t.string :ua_os
      t.string :geoip_country
      t.string :geoip_state
      t.string :geoip_city

      t.timestamps null: false
    end
  end
end
