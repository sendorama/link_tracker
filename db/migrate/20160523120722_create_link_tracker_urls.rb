class CreateLinkTrackerUrls < ActiveRecord::Migration
  def change
    create_table :link_tracker_urls do |t|
      t.text :location
      t.integer :link_count

      t.timestamps null: false
    end
    add_index :link_tracker_urls, :location, unique: true, length: 100
  end
end
