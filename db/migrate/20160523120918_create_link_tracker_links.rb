class CreateLinkTrackerLinks < ActiveRecord::Migration
  def change
    create_table :link_tracker_links do |t|
      t.string :unique_key
      t.string :campaign_id
      t.string :destination_id
      t.integer :visits_count
      t.integer :last_visited_at
      t.belongs_to :url

      t.timestamps null: false
    end

    add_index :link_tracker_links, :unique_key, unique: true
    add_index :link_tracker_links, [:campaign_id, :destination_id]
  end
end
