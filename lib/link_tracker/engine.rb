module LinkTracker
  class Engine < ::Rails::Engine
    isolate_namespace LinkTracker

    # Precompile assets. Because you need to. Do not, put the compile in the main app!
    initializer 'link_tracker.assets.precompile' do |app|
      config.assets.precompile += %w( link_tracker/*.css link_tracker/*.js )
    end

    # Precompile manifests
    initializer 'link_tracker.asset_precompile_paths' do |app|
      app.config.assets.precompile += ["link_tracker/manifests/*"]
    end
  end
end
