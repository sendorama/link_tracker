require 'rails_helper'
require 'faker'
require 'byebug'
require 'timecop'

describe LinkTracker::LinksController, type: :controller do
  routes { LinkTracker::Engine.routes }

  
  describe "GET visit" do
    let(:campaign_id){ rand(9999) }
    let(:dstination_id){ rand(9999) }
    let(:an_url){Faker::Internet.url}

    let(:link) { LinkTracker::Link.register(an_url, campaign_id, dstination_id) }
    before do
      get :visit, unique_key: link.unique_key
    end
    it { expect(response).to redirect_to(link.url.location) }
  end

  describe "POST create" do
    let(:campaign_id){ rand(9999) }
    let(:dstination_id){ rand(9999) }
    let(:an_url){Faker::Internet.url}

    before do
      post :create, url: an_url, campaign_id: campaign_id, destination_id: dstination_id
    end
    it { expect(response.body).to be_a_short_url }
  end

  describe "for some campaign with links" do

     # some links
    let!(:lgc1d1) { LinkTracker::Link.register('http://gmail.com', :c1, :d1) }
    let!(:lgc1d2) { LinkTracker::Link.register('http://gmail.com', :c1, :d2) }
    let!(:lyc1d1) { LinkTracker::Link.register('http://yahoo.com', :c1, :d1) }
    let!(:lyc1d2) { LinkTracker::Link.register('http://yahoo.com', :c1, :d2) }
    let!(:lyc2d2) { LinkTracker::Link.register('http://yahoo.com', :c2, :d2) }
    
    let(:google) {lgc1d1.url_id}
    let(:yahoo) {lyc1d1.url_id}

    before do
      Timecop.freeze(Time.local(2016, 9, 1, 12, 0, 0)) do
      Sidekiq::Testing.inline! do
        get :visit, unique_key: lgc1d1.unique_key
        get :visit, unique_key: lgc1d1.unique_key
        get :visit, unique_key: lgc1d2.unique_key
        get :visit, unique_key: lyc2d2.unique_key
      end
      end
    end

    describe "GET report" do
      before do
        get :report, campaign_id: :c1
      end
      it { expect(LinkTracker::Visit.count).to eq(4) }
      
      it { expect(JSON.parse(response.body)).to eq [
        {'url' => 'http://gmail.com/', 'url_id' => google, 'count' => 2},
        {'url' => 'http://yahoo.com/', 'url_id' => yahoo, 'count' => 0}
      ] }
    end

    describe "GET history" do
      before do
        get :history, campaign_id: :c1, url_id: google
      end
      it { expect(LinkTracker::Visit.count).to eq(4) }
      it { expect(JSON.parse(response.body)).to eq [
        {'destination_id' => 'd1', 'url_id' => google, 'visited_at' => '2016-09-01T15:00:00.000Z'},
        {'destination_id' => 'd1', 'url_id' => google, 'visited_at' => '2016-09-01T15:00:00.000Z'},
        {'destination_id' => 'd2', 'url_id' => google, 'visited_at' => '2016-09-01T15:00:00.000Z'},
      ] }
    end
  end

end
