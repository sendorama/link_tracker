require 'rails_helper'
require 'rspec/expectations'

RSpec::Matchers.define :be_a_short_url do
  match do |actual|
    actual_url = actual.to_s
    actual_url =~ URI::DEFAULT_PARSER.regexp[:ABS_URI] && actual_url.size <= 22
  end
end

describe LinkTracker::Link do
  it {expect(subject).to be_instance_of described_class }

  describe "register" do
    let(:some_url) {'http://uol.com'}
    let(:another_url) {'http://aol.com'}
    let(:campaign_id) {1111}
    let(:destination_id) {2222}

    it { expect(described_class.register(some_url)).to be_a_short_url }
    it { expect(described_class.register(some_url)).to_not eq described_class.register(another_url) }
    it { expect(described_class.register(some_url)).to_not eq some_url}
    #it { expect(described_class.register(some_url)).to eq described_class.register(some_url) }
    it { expect(described_class.register(some_url, campaign_id)).to be_a_short_url }
    it { expect(described_class.register(some_url, campaign_id, destination_id)).to be_a_short_url }
  end


  describe "#register_visit" do
    let(:user_agent) { 'Mozilla/5.0 (Windows NT 6.4; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36'}
    it do
      expect{ subject.register_visit('8.8.8.8', user_agent) }.to change{ Sidekiq::Extensions::DelayedClass.jobs.size}.by 1
    end
  end
end
