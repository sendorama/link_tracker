require 'rails_helper'

describe LinkTracker::Visit do
  describe "::register(link_id, remote_ip, user_agent)" do
    let(:link){ LinkTracker::Link.register(Faker::Internet.url) }
    it { expect{described_class.register(link.id, '8.8.8.8', 'Some User Agent')}.to change(described_class, :count).by(1) }
  end
end
