$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "link_tracker/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "link_tracker"
  s.version     = LinkTracker::VERSION
  s.authors     = ["Romeu Fonseca"]
  s.email       = ["romeu.hcf@gmail.com"]
  s.homepage    = "https://bitbucket.org/sendorama/link_tracker"
  s.summary     = "Pending: Summary of LinkTracker."
  s.description = "Pending: Description of LinkTracker."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.6"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "simplecov"
  s.add_development_dependency "faker"
  s.add_development_dependency "timecop"
end
