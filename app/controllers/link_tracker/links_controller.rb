module LinkTracker
  class LinksController < ApplicationController
    skip_before_action :verify_authenticity_token
    def visit
      link = Link.includes(:url).find_by(unique_key: params[:unique_key])
      if link
        link.register_visit(request.remote_ip, request.user_agent)
        redirect_to link.url.location, status: 301
      else
        render :text => 'Not Found', :status => '404'
      end
    end

    def create
      link = Link.register(params[:url], params[:campaign_id], params[:destination_id])

      render text: link.to_s
    end

    def report
      scope = Link.includes(:visits).joins(:url)
      scope = scope.where(campaign_id: params[:campaign_id]) if params[:campaign_id]
      scope = scope.where(destination_id: params[:destination_id]) if params[:destination_id]

      result = scope.group(:location, :url_id).count('DISTINCT ( CASE WHEN link_tracker_visits.id IS NOT NULL THEN destination_id END)')
      
      render json: result.map{|k,v| {url:k.first, url_id: k.last, count: v} }
    end

    def history
      scope = Link.joins(:visits)
      scope = scope.where(campaign_id: params[:campaign_id]) if params[:campaign_id]
      scope = scope.where(destination_id: params[:destination_id] ) if params[:destination_id]
      scope = scope.where(url_id: params[:url_id] ) if params[:url_id]
      scope = scope.select('link_tracker_links.url_id, link_tracker_visits.created_at as visited_at, destination_id')

      render plain: scope.to_json(except: :id)
    end
  end
end
