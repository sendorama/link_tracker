module LinkTracker
  class Url < ActiveRecord::Base
    validates :location, uniqueness: true,
      presence: true,
      format: {with: URI::DEFAULT_PARSER.regexp[:ABS_URI] }
    has_many :links, counter_cache: true, dependent: :destroy
  end
end
