module LinkTracker
  class Link < ActiveRecord::Base
    belongs_to :url, touch: true
    has_many :visits, dependent: :destroy

    def to_s
      File.join(base_url, unique_key)
    end

    def base_url
      ['http://' , ENV['LINK_TRACKER_DOMAIN']].join()
    end

    REGEX_LINK_HAS_PROTOCOL = Regexp.new('\Ahttp:\/\/|\Ahttps:\/\/', Regexp::IGNORECASE)

    def self.register(destination_url, cid=nil, did=nil)
      url = Url.create!(location: clean_url(destination_url)) rescue Url.find_by(location: clean_url(destination_url))

      loop do
        begin
          return self.create!(
            url_id: url.id,
            campaign_id: cid,
            destination_id: did,
            unique_key: generate_unique_key
          )
        rescue ActiveRecord::RecordNotUnique, ActiveRecord::StatementInvalid
          logger.info("Failed to generate ShortenedUrl with unique_key: #{unique_key}")
          self.unique_key = nil
          if (count +=1) < 5
            logger.info("retrying with different unique key")
            retry
          else
            logger.info("too many retries, giving up")
            raise
          end
        end
      end
    end

    def register_visit(remote_ip, user_agent)
      Visit.delay.register(self.id, remote_ip, user_agent)
    end

    private
    def self.generate_unique_key
      charset = 'qwertyuiopasfghjklzxcvbnm12347890'.split('')
      size = 3 + rand(5)
      (0...size).map{ charset[rand(charset.size)] }.join
    end

    def self.clean_url(url)
      url = url.to_s.strip
      if url !~ REGEX_LINK_HAS_PROTOCOL && url[0] != '/'
        url = "/#{url}"
      end
      URI.parse(url).normalize.to_s
    end
  end
end
