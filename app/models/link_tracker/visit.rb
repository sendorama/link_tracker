module LinkTracker
  class Visit < ActiveRecord::Base
    belongs_to :link

    def self.register(link_id, remote_ip, user_agent)
      create!(link: Link.find(link_id), ip_address: remote_ip, user_agent: user_agent)
    end
  end
end
