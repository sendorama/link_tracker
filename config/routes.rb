class DomainConstraint
  def initialize(*domains)
    @domains = [domains].flatten
  end

  def matches?(request)
    @domains.include? request.domain
  end
end

class SubdomainConstraint
  def initialize(*subdomains)
    @subdomains = [subdomains].flatten
  end

  def self.matches?(request)
    request.subdomain.present? && @subdomains.include?(request.subdomain)
  end
end

LinkTracker::Engine.routes.draw do
  post '/link_tracker' => 'links#create'
  get '/link_tracker/report' => 'links#report'
  get '/link_tracker/history' => 'links#history'
  get '/link_tracker/visit/:unique_key' => "links#visit"

  constraints(DomainConstraint.new(ENV['LINK_TRACKER_DOMAIN'])) do
    get '/:unique_key' => "links#visit"
  end
end
